import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { SharedMaterialModule } from './shared-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './component/home/home.component';
import { MocListComponent } from './component/moc-list/moc-list.component';
import { PartFinderComponent } from './component/part-finder/part-finder.component';
import { httpInterceptorsProviders } from './service/interceptor';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { PartPanelComponent } from './component/_shared/part-panel/part-panel.component';
import { SigninComponent } from './component/_shared/dialog/signin/signin.component';
import { RegisterComponent } from './component/register/register.component';
import { SnackbarComponent } from './component/_shared/snackbar/snackbar.component';
import { MocAddComponent } from './component/moc-add/moc-add.component';
import { DragAndDropDirective } from './directive/drag-and-drop.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    MocListComponent,
    PartFinderComponent,
    PartPanelComponent,
    SigninComponent,
    RegisterComponent,
    SnackbarComponent,
    MocAddComponent,
    DragAndDropDirective,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedMaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ],
  providers: [
    httpInterceptorsProviders,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
