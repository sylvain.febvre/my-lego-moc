import { NgModule } from '@angular/core';
import { FileInputComponent } from './file-input/file-input.component';
import { ByteFormatPipe } from './pipe/byte-format.pipe';



@NgModule({
  declarations: [FileInputComponent, ByteFormatPipe],
  exports: [FileInputComponent, ByteFormatPipe],
})
export class AshMatFileInputModule { }
