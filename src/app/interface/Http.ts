import { HttpErrorResponse } from '@angular/common/http';

export interface HttpBackendResponse {
  success: boolean,
  message: string,
}

export interface HttpBackendErrorResponse extends HttpBackendResponse {
  error: {
    name: HttpErrorNames,
    fullUrl: string,
    host: string,
    message: string,
    method: string,
    protocol: string,
    statusCode: number,
    url: string,
  }
}

export interface HttpBackendError extends HttpErrorResponse {
  error: HttpBackendErrorResponse,
}

export enum HttpErrorNames {
  NO_USER = 1,
  WRONG_PWD = 2,
  USERNAME_EXISTS = 3,
  PWD_TOO_SHORT = 4,
  WRONG_PWDCHECK = 5,

  MISSING_USERNAME = 100,
  MISSING_PWD = 101,
  MISSING_PWDCHECK = 102,
}