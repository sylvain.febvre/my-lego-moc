export interface IMoc {
  id: number,
  id_user: number,
  username: string,
  img_url: string,
  studio_url: string,
  name: string,
  description: string,
  download: number,
  vote: number,
  view: number,
  created: Date,
}