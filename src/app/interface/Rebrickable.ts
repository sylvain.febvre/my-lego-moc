export interface IRebrickableResults<ResultType> {
    count: number,
    next: string,
    previous: string,
    results: ResultType[],
}

export interface IPart {
    part_num: string,
    name: string,
    part_cat_id: number,
    part_url: string,
    part_img_url: string,
}

export interface IColor {
    id: number,
    name: string,
    rgb: string,
    is_trans: boolean,
}

export interface IPartCat {
    id: number,
    name: string,
    part_count: number,
}

export interface IPartColor {
    color_id: number,
    color_name: string,
    num_sets: number,
    num_set_parts: number,
    part_img_url: string,
}

export interface ICategory {
    id: number,
    name: string,
    part_count: number,
}
