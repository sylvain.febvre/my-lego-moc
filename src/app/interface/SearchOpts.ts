export interface ISearchOpts {
    pageSize?: number,
    colorId?: number,
    search: string,
    ordering?: string,
    page: number,
    partCatId?: number,
}