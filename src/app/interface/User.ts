export interface UserToken {
    id: number,
    username: string,
    role: Role,
    exp: any,
}

export interface UserLogin {
    username: string,
    password: string,
    passwordCheck?: string,
}

export enum Role {
    Admin = 0,
    User = 1,
}