export interface SnackBarData {
  title: string,
  message: string,
  type: 'success' | 'info' | 'error' | 'help' | 'warning',
  duration?: number,
}