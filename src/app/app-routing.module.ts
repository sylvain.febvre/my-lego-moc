import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { MocListComponent } from './component/moc-list/moc-list.component';
import { PartFinderComponent } from './component/part-finder/part-finder.component';
import { ColorListResolverService } from './service/resolver/color-list-resolver.service';
import { PartCatListResolverService } from './service/resolver/part-cat-list-resolver.service';
import { RegisterComponent } from './component/register/register.component';
import { NoAuthGuard } from './service/guard/no-auth.guard';
import { AuthGuard } from './service/guard/auth.guard';
import { UserMocListResolverService } from './service/resolver/user-moc-list-resolver.service';
import { MocAddComponent } from './component/moc-add/moc-add.component';


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'moc',
    canActivate: [AuthGuard],
    runGuardsAndResolvers: 'always',
    children: [
      {
        path: '',
        component: MocListComponent,
        resolve: {
          mocs: UserMocListResolverService,
        },
        pathMatch: 'full'
      },
      {
        path: 'add',
        component: MocAddComponent,
      }
]
  },
{
  path: 'api',
    component: PartFinderComponent,
      resolve: {
    colors: ColorListResolverService,
      partCats: PartCatListResolverService,
    },
},
{
  path: 'register',
    component: RegisterComponent,
      canActivate: [NoAuthGuard],
        runGuardsAndResolvers: 'always',
  },
{
  path: '**',
    pathMatch: 'full',
      redirectTo: 'home',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
