import { Component, HostBinding } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
@Component({
  selector: 'mlm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  host: {
    class: 'mat-app-background'
  }
})
export class AppComponent {
  pageLoading = false;

  @HostBinding('class.mlm-dark-theme')
  get isDark(): boolean {
    return coerceBooleanProperty(localStorage.getItem('is_dark_theme'));
  }

  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      switch(true) {
        case event instanceof NavigationStart: {
          this.pageLoading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.pageLoading = false;
          break;
        }
        
        default: {
          break;
        }
      }
    });
  }
}
