import { Component } from '@angular/core';
import { ValidatorFn, AbstractControl, ValidationErrors, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DialogManagerService } from 'src/app/service/dialog-manager.service';
import { UserService } from 'src/app/service/api/user.service';

@Component({
  selector: 'mlm-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  host: { class: 'mlm-page' }
})
export class RegisterComponent {

  compareValidator(controlNameToCompare: string): ValidatorFn {
    return (c: AbstractControl): ValidationErrors | null => {
      const controlToCompare = c.root.get(controlNameToCompare);
      if ((controlToCompare === null || controlToCompare.value?.length === 0) && (c.value === null || c.value?.length) === 0) {
        return null;
      }
      return controlToCompare && controlToCompare.value !== c.value ? { 'compare': true } : null;
    };
  }

  newUserForm = this.fb.group({
    username: ['', [
      Validators.required,
      Validators.minLength(4),
    ]],
    password: ['', [
      Validators.required,
      Validators.minLength(8),
    ]],
    passwordCheck: ['', [
      Validators.required,
      Validators.minLength(8),
      this.compareValidator('password'),
    ]]
  });

  get usernameInput(): FormControl { return this.newUserForm.get('username') as FormControl; }
  get passwordInput(): FormControl { return this.newUserForm.get('password') as FormControl; }
  get passwordCheckInput(): FormControl { return this.newUserForm.get('passwordCheck') as FormControl; }

  constructor(
    private fb: FormBuilder,
    private user: UserService,
    public dialog: DialogManagerService,
  ) { }

  onSubmit() {
    this.user.create(this.newUserForm.value).subscribe();
  }

}
