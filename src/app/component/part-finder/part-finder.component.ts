import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map, startWith } from 'rxjs/operators';
import { FormBuilder, FormControl } from '@angular/forms';
import { RebrickableService } from 'src/app/service/api/rebrickable.service';
import { IPart, IColor, IPartCat } from 'src/app/interface/Rebrickable';
import { ActivatedRoute } from '@angular/router';
import { ISearchOpts } from 'src/app/interface/SearchOpts';

@Component({
  selector: 'mlm-part-finder',
  templateUrl: './part-finder.component.html',
  styleUrls: ['./part-finder.component.scss'],
  host: { class: 'mlm-page' },
})
export class PartFinderComponent implements OnInit {

  @ViewChild('content') contentRef: ElementRef<HTMLDivElement>;

  resultCount: number = 0;
  loading: boolean = false;

  searchOpts: ISearchOpts = {
    page: 1,
    search: '',
  }

  partCats: IPartCat[];
  colors: IColor[];

  readonly searchForm = this.fb.group({
    search: [''],
    category: [''],
    color: [''],
    ordering: ['name'],
    pageSize: ['20'],
  })

  get searchInput(): FormControl {
    return this.searchForm.get('search') as FormControl;
  }

  get categoryInput(): FormControl {
    return this.searchForm.get('category') as FormControl;
  }

  get colorInput(): FormControl {
    return this.searchForm.get('color') as FormControl;
  }

  get orderingInput(): FormControl {
    return this.searchForm.get('ordering') as FormControl;
  }
  
  get pageSizeInput(): FormControl {
    return this.searchForm.get('pageSize') as FormControl;
  }

  filteredCats$: Observable<IPartCat[]>;
  filteredColors$: Observable<IColor[]>;

  public searchResult$: Observable<IPart[]>;

  constructor(
    private rebrickable: RebrickableService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe((data: { colors: IColor[], partCats: IPartCat[] }) => {
      this.partCats = data.partCats;
      this.colors = data.colors;
    });
    this.filteredCats$ = this.categoryInput.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => this.partCats.filter(partCat => partCat.name.toLowerCase().includes(name.toLowerCase()))),
    );
    this.filteredColors$ = this.colorInput.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => this.colors.filter(color => color.name.toLowerCase().includes(name.toLowerCase()))),
    )
  }

  onSubmit() {
    this.searchOpts.search = this.searchInput.value;
    this.searchOpts.pageSize = this.pageSizeInput.value;
    this.searchOpts.ordering = this.orderingInput.value;

    if(typeof this.categoryInput.value !== 'string') {
      this.searchOpts.partCatId = this.categoryInput.value.id;
    } else {
      this.searchOpts.partCatId = null;
    }

    if(typeof this.colorInput.value !== 'string') {
      this.searchOpts.colorId = this.colorInput.value.id;
    } else {
      this.searchOpts.colorId = null;
    }

    this.getPage(1);
  }

  onReset() {
    this.searchInput.setValue('');
    this.categoryInput.setValue('');
    this.colorInput.setValue('');
    this.orderingInput.setValue('name');
    this.pageSizeInput.setValue('20');
  }

  getPage(page: number) {
    this.loading = true;
    this.searchOpts.page = page;
    this.searchResult$ = this.rebrickable.getPartList(this.searchOpts).pipe(
      tap(res => {
        this.resultCount = res.count;
        this.loading = false;
        this.contentRef.nativeElement.scrollTo({ top: 0 });
      }),
      map(res => res.results),
    )
  }

  display(value: IPartCat | IColor) {
    return value.name;
  }

}
