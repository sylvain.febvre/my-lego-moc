import { Component } from '@angular/core';
import { UserService } from 'src/app/service/api/user.service';
import { Observable } from 'rxjs';
import { UserToken } from 'src/app/interface/User';
import { DialogManagerService } from 'src/app/service/dialog-manager.service';

@Component({
  selector: 'mlm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  get user$(): Observable<UserToken> {
    return this.auth.currentUser$;
  }

  constructor(
    public auth: UserService,
    public dialog: DialogManagerService,
  ) { }


}
