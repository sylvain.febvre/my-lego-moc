import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { SnackbarManagerService } from 'src/app/service/snackbar-manager.service';
import { FileValidators } from 'src/app/modules/ash-mat-file-input';


@Component({
  selector: 'mlm-moc-add',
  templateUrl: './moc-add.component.html',
  styleUrls: ['./moc-add.component.scss'],
  host: { class: 'mlm-page' },
})
export class MocAddComponent implements OnInit {

  stepOne = this._fb.group({
    file: ['', [Validators.required, FileValidators.maxFileSize(2 * 1024 * 1024), FileValidators.extension('.io')]],
    name: ['', Validators.required],
    desc: [''],
    private: [false],
  })

  get name(): FormControl { return this.stepOne.get('name') as FormControl }
  get file(): FormControl { return this.stepOne.get('file') as FormControl }

  imageFiles: {
    file: File,
    img: ArrayBuffer | string,
  }[] = [];

  constructor(
    private _fb: FormBuilder,
    private _snackbar: SnackbarManagerService,
  ) {
    this.file.valueChanges.subscribe(value => {
      if (this.file.valid && !this.name.value) {
        this.name.patchValue(value[0].name.replace(/\.[^/.]+$/, ""))
      }
    })
  }

  ngOnInit(): void {
  }

  addImages(images: FileList) {
    const imgError = [];
    for (let i = 0; i < images.length; i++) {
      const image = images.item(i);
      console.log(image)
      if (image.size < 2 * 1024 * 1024) {
        var reader = new FileReader();
        reader.onload = e => {
          this.imageFiles.push({
            file: image,
            img: e.target.result,
          })
        }
        reader.readAsDataURL(image);
      } else {
        imgError.push({
          error: "size",
          name: image.name,
        })
      }
    }
    if (imgError.length > 0) {
      this._snackbar.openSnackBar({
        title: 'Images upload',
        message: `${imgError.length} files was too big and ignored`,
        type: "warning",
      })
    }
  }

  removeImage(index: number) {
    this.imageFiles.splice(index, 1);
  }
}
