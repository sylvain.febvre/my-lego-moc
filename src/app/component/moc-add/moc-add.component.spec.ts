import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MocAddComponent } from './moc-add.component';

describe('MocAddComponent', () => {
  let component: MocAddComponent;
  let fixture: ComponentFixture<MocAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MocAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MocAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
