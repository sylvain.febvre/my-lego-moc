import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IMoc } from 'src/app/interface/Moc';
import { map, take, tap } from 'rxjs/operators';

@Component({
  selector: 'mlm-moc-list',
  templateUrl: './moc-list.component.html',
  styleUrls: ['./moc-list.component.scss'],
  host: { class: 'mlm-page' },
})
export class MocListComponent implements OnInit {

  constructor(
    private _route: ActivatedRoute,
  ) { }

  mocList: []

  ngOnInit(): void {
    this._route.data.subscribe((data) => {
      this.mocList = data.mocs;
    })
  }

}
