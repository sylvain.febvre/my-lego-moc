import { Component, Input } from '@angular/core';
import { IPart, IPartColor, ICategory } from 'src/app/interface/Rebrickable';
import { forkJoin, Observable } from 'rxjs';
import { RebrickableService } from 'src/app/service/api/rebrickable.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from 'src/app/service/api/user.service';
import { UserToken } from 'src/app/interface/User';

@Component({
  selector: 'mlm-part-panel',
  templateUrl: './part-panel.component.html',
  styleUrls: ['./part-panel.component.scss']
})
export class PartPanelComponent {

  @Input() part: IPart;

  addPartsForm = 
    this.fb.group({
      color: ['', Validators.required],
      list: ['my', Validators.required],
      condition: ['new'],
      qty: [1, Validators.min(1)],
    });

  partColors: IPartColor[];
  category: ICategory;

  get colorInput(): FormControl {
    return this.addPartsForm.get('color') as FormControl;
  }

  get imgBigUrl(): string {
    return this.colorInput.value.part_img_url || this.part.part_img_url; 
  }

  get user$(): Observable<UserToken> {
    return this.auth.currentUser$;
  }

  constructor(
    private rebrickable: RebrickableService,
    private fb: FormBuilder,
    private auth: UserService,
  ) { }

  onOpened() {
    if (!(this.partColors || this.category)) {
      forkJoin(
        this.rebrickable.getAllPartColors(this.part.part_num),
        this.rebrickable.getCategory(this.part.part_cat_id),
      ).subscribe(
        result => {
          this.partColors = result[0].results;
          this.category = result[1];
        }
      )
    }

  }

}
