import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartPanelComponent } from './part-panel.component';

describe('PartPanelComponent', () => {
  let component: PartPanelComponent;
  let fixture: ComponentFixture<PartPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
