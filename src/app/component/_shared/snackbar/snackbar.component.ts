import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';
import { SnackBarData } from 'src/app/interface/Snackbar';

@Component({
  selector: 'mlm-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {
  icon: string = 'info';

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: SnackBarData,
    public snackbarRef: MatSnackBarRef<SnackbarComponent>,
  ) { }

  ngOnInit(): void {
    this.icon = this.data.type === "success" ? 'check_circle' : this.data.type;
  }

}
