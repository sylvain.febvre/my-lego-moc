import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from 'src/app/service/api/user.service';
import { SnackbarManagerService } from 'src/app/service/snackbar-manager.service';
import { MatDialogRef } from '@angular/material/dialog';
import { HttpBackendResponse, HttpBackendErrorResponse, HttpBackendError, HttpErrorNames } from 'src/app/interface/Http';
import { Router } from '@angular/router';

@Component({
  selector: 'mlm-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  signinForm = this.fb.group({
    username: ['', [Validators.required, Validators.minLength(4)]],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });

  get username(): FormControl {
    return this.signinForm.get('username') as FormControl;
  }

  get password(): FormControl {
    return this.signinForm.get('password') as FormControl;
  }

  constructor(
    private fb: FormBuilder,
    private user: UserService,
    private _dialogRef: MatDialogRef<SigninComponent>,
  ) { }

  ngOnInit(): void {
  }

  signin() {
    this.user.signin(this.signinForm.value).subscribe(
      () => this._dialogRef.close(),
      (res: HttpBackendError) => {
        if(res.error.error.name === HttpErrorNames.NO_USER) {
          this.username.setErrors({
            noUser: true,
          });
        } else if (res.error.error.name === HttpErrorNames.WRONG_PWD) {
          this.password.setErrors({
            wrongPwd: true,
          });
        }
      }
    )
  }

}
