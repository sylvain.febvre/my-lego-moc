import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserToken, UserLogin } from 'src/app/interface/User';
import { HttpClient } from '@angular/common/http';
import { map, distinctUntilChanged, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SnackbarManagerService } from '../snackbar-manager.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _jwtHelper = new JwtHelperService();
  private _currentUser$: BehaviorSubject<UserToken>;
  public currentUser$: Observable<UserToken>;

  get token(): string {
    return localStorage.getItem('access_token');
  }

  set token(token: string) {
    localStorage.setItem('access_token', token);
  }

  private get decodedToken(): UserToken {
    if(this.token) {
      try {
        const decodedToken = this._jwtHelper.decodeToken(this.token);
        return !this._jwtHelper.isTokenExpired(this.token) ? decodedToken : this.removeToken();
      } catch (error) {
        return this.removeToken();
      }
    }
    return null;
  }

  private removeToken() {
    localStorage.removeItem('access_token');
    return null;
  }

  constructor(
    private _http: HttpClient,
    private _snackbar: SnackbarManagerService,
    private _router: Router,
  ) {
    this._currentUser$ = new BehaviorSubject(this.decodedToken);
    this.currentUser$ = this._currentUser$.asObservable().pipe(
      distinctUntilChanged(),
    );
  }

  signin(credential: UserLogin): Observable<UserToken> {
    return this._http.post(`${environment.backendUrl}/user/auth`, credential).pipe(
      map((res: any): UserToken => {
        this.token = res.token;
        this._currentUser$.next(this.decodedToken);
        return this.decodedToken;
      }),
      tap(user => {
        this._snackbar.openSnackBar({
          title: 'Login successful',
          type: 'success',
          message: `Welcome back ${user.username} !`
        });
        this._router.navigateByUrl(this._router.url);
      })
    )
  }

  signoff() {
    const username = this.decodedToken.username;
    localStorage.removeItem('access_token');
    this._currentUser$.next(null);
    this._snackbar.openSnackBar({
      title: 'Logout successful',
      type: 'info',
      message: `Bye ${username} !`
    });
    this._router.navigateByUrl(this._router.url);
  }

  create(user: UserLogin ): Observable<any> {
    return this._http.post(`${environment.backendUrl}/user/create`, user)
  }
}
