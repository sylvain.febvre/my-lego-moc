import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { IMoc } from 'src/app/interface/Moc';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(
    private _http: HttpClient,
  ) { }

  getUserMocList(userId: number): Observable<IMoc[]> {
    return this._http.get<IMoc[]>(`${environment.backendUrl}/user/${userId}/mocs`)
  }
}
