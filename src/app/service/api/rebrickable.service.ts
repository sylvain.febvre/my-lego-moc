import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISearchOpts } from 'src/app/interface/SearchOpts';
import { Observable } from 'rxjs';
import { IRebrickableResults, IPart, IColor, IPartCat, IPartColor, ICategory } from 'src/app/interface/Rebrickable';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RebrickableService {

  constructor(
    private http: HttpClient,
  ) { }

  getPartList(config: ISearchOpts): Observable<IRebrickableResults<IPart>> {
    const colorIdParam = config.colorId ? `color_id=${config.colorId}` : '';
    const partCatIdParam = config.partCatId ? `part_cat_id=${config.partCatId}` : '';
    const orderingParam = `ordering=${config.ordering ? config.ordering : 'name'}`;
    const pageParam = `page=${config.page ? config.page : 1}`;
    const pageSizeParam = `page_size=${config.pageSize ? config.pageSize : 10}`;
    const searchParam = `search=${config.search}`;

    const query = `${environment.rebrickApiUrl}/parts/?${colorIdParam}&${partCatIdParam}&${orderingParam}&${pageParam}&${pageSizeParam}&${searchParam}`;

    return this.http.get<IRebrickableResults<IPart>>(query);
  }

  getAllcolors(): Observable<IRebrickableResults<IColor>> {
    const query = `${environment.rebrickApiUrl}/colors/?ordering=name&page_size=200`;

    return this.http.get<IRebrickableResults<IColor>>(query);
  }

  getAllPartCats(): Observable<IRebrickableResults<IPartCat>> {
    const query = `${environment.rebrickApiUrl}/part_categories/?ordering=name`;

    return this.http.get<IRebrickableResults<IPartCat>>(query);
  }

  getAllPartColors(partNum: string, inSet: boolean = true): Observable<IRebrickableResults<IPartColor>> {
    const query = `${environment.rebrickApiUrl}/parts/${partNum}/colors/?ordering=name&page_size=200`;

    return this.http.get<IRebrickableResults<IPartColor>>(query).pipe(
      map(result => {
        if(inSet) {
          result.results = result.results
            .filter(partColor => partColor.num_sets > 0)
            .sort((a, b) => (a.color_name.toLowerCase() > b.color_name.toLowerCase()) ? 1 : -1);
        }
        return result;
      })
    )
  }

  getCategory(catId: number): Observable<ICategory> {
    const query = `${environment.rebrickApiUrl}/part_categories/${catId}/`;

    return this.http.get<ICategory>(query);
  }
}
