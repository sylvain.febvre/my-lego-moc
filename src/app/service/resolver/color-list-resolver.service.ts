import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { IColor } from 'src/app/interface/Rebrickable';
import { RebrickableService } from '../api/rebrickable.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ColorListResolverService implements Resolve<Observable<IColor[]>> {

  constructor(
    private rebrickable: RebrickableService,
  ) { }

  resolve(): Observable<IColor[]> {
    return this.rebrickable.getAllcolors().pipe(
      map(result => result.results),
    );
  }

}
