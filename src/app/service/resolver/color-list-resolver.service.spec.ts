import { TestBed } from '@angular/core/testing';

import { ColorListResolverService } from './color-list-resolver.service';

describe('ColorListResolverService', () => {
  let service: ColorListResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ColorListResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
