import { TestBed } from '@angular/core/testing';

import { UserMocListResolverService } from './user-moc-list-resolver.service';

describe('UserMocListService', () => {
  let service: UserMocListResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserMocListResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
