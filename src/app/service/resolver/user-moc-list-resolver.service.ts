import { Injectable } from '@angular/core';
import { BackendService } from '../api/backend.service';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { IMoc } from 'src/app/interface/Moc';
import { UserService } from '../api/user.service';
import { concatMap, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserMocListResolverService implements Resolve<Observable<IMoc[]>>{

  constructor(
    private _backend: BackendService,
    private _user: UserService,
  ) { }

  resolve(): Observable<IMoc[]> {
    return this._user.currentUser$.pipe(
      take(1),
      concatMap(user => this._backend.getUserMocList(user.id))
    );
  }
}
