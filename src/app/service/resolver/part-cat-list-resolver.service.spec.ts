import { TestBed } from '@angular/core/testing';

import { PartCatListResolverService } from './part-cat-list-resolver.service';

describe('PartCatListResolverService', () => {
  let service: PartCatListResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartCatListResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
