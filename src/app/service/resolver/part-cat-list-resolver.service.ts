import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { IPartCat } from 'src/app/interface/Rebrickable';
import { RebrickableService } from '../api/rebrickable.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PartCatListResolverService implements Resolve<Observable<IPartCat[]>> {

  constructor(
    private rebrickable: RebrickableService,
  ) { }

  resolve(): Observable<IPartCat[]> {
    return this.rebrickable.getAllPartCats().pipe(
      map(result => result.results),
    );
  }

}
