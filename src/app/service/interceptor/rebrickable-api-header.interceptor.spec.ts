import { TestBed } from '@angular/core/testing';

import { RebrickableApiHeaderInterceptor } from './rebrickable-api-header.interceptor';

describe('RebrickableApiHeaderInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      RebrickableApiHeaderInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: RebrickableApiHeaderInterceptor = TestBed.inject(RebrickableApiHeaderInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
