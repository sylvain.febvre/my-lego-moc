import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Provider } from '@angular/core';
import { RebrickableApiHeaderInterceptor } from './rebrickable-api-header.interceptor';
import { ResponseErrorInterceptor } from './response-error.interceptor';
import { BackendHeaderInterceptor } from './backend-header.interceptor';


export const httpInterceptorsProviders: Provider[] = [
    { provide: HTTP_INTERCEPTORS, useClass: RebrickableApiHeaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: BackendHeaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ResponseErrorInterceptor, multi: true },
]