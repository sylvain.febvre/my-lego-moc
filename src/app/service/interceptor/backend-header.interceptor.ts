import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserService } from '../api/user.service';

@Injectable()
export class BackendHeaderInterceptor implements HttpInterceptor {

  constructor(
    private _user: UserService,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(request.url.startsWith(environment.backendUrl)) {
      request = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${this._user.token}`),
      });
    }

    return next.handle(request);
  }
}
