import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpBackendError } from 'src/app/interface/Http';
import { SnackbarManagerService } from '../snackbar-manager.service';

@Injectable()
export class ResponseErrorInterceptor implements HttpInterceptor {

  constructor(
    private _snackbar: SnackbarManagerService,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((err: HttpBackendError) => {
        if([400, 500].includes(err.status)) {
          this._snackbar.openSnackBar({
            title: `Error ${err.status} : ${err.statusText}`,
            message: err.error.message || err.url,
            type: "error",
            duration: -1,
          });
        }
        return throwError(err);
      }),
    );
  }
}
