import { Injectable } from '@angular/core';
import { SnackBarData } from '../interface/Snackbar';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from '../component/_shared/snackbar/snackbar.component';

@Injectable({
  providedIn: 'root'
})
export class SnackbarManagerService {

  constructor(
    private _snackbar: MatSnackBar,
  ) { }

  openSnackBar(data: SnackBarData) {
    const config: MatSnackBarConfig<SnackBarData> = {
      duration: data.duration || 2500,
      data,
      horizontalPosition: "center",
      verticalPosition: "bottom",
      panelClass: [`mlm-snackbar`, `mlm-${data.type}`],
    }
    this._snackbar.openFromComponent(SnackbarComponent, config)
  }
}
