import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SigninComponent } from '../component/_shared/dialog/signin/signin.component';

@Injectable({
  providedIn: 'root'
})
export class DialogManagerService {

  constructor(
    private dialog: MatDialog,
  ) { }

  openSignIn() {
    this.dialog.open(SigninComponent, {
      width: '400px',
      autoFocus: false,
      position: {
        top: '200px'
      },
      closeOnNavigation: true,
    })
  }
}
