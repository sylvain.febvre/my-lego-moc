import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../api/user.service';
import { map } from 'rxjs/operators';
import { SnackbarManagerService } from '../snackbar-manager.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private _user: UserService,
    private _router: Router,
    private _snackbar: SnackbarManagerService,
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this._user.currentUser$.pipe(
      map(user => {
        if (user) {
          return true;
        }
        this._snackbar.openSnackBar({
          title: 'Not authorized',
          message: 'You must be logged to access this page',
          type: 'warning',
          duration: 2000,
        });
        this._router.navigate(['/home']);
        return false;
      }),
    );
  }

}
